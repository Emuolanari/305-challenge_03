from google.appengine.api import users
from google.appengine.ext import ndb
import webapp2
import jinja2
import os

JINJA_ENVIRONMENT = jinja2.Environment(
    loader=jinja2.FileSystemLoader(os.path.join(os.path.dirname(__file__),'templates')),
    extensions=['jinja2.ext.autoescape'])

# A Model for a User
class ChatMember(ndb.Model):
  account = ndb.StringProperty()
  name = ndb.StringProperty()

# A Model for a ChatMessage
class ChatMessage(ndb.Model):
  user = ndb.StringProperty()
  text = ndb.StringProperty()
  created = ndb.DateTimeProperty(auto_now=True)
  latlonpos = ndb.StringProperty()
  place = ndb.StringProperty()
  city = ndb.StringProperty()
  
# A helper to do the rendering and to add the necessary
# variables for the _base.htm template
def doRender(handler, tname = 'index.htm', template_values = { }):
  template = JINJA_ENVIRONMENT.get_template(tname)
  # Make a copy of the dictionary and add the path and session
  newval = dict(template_values)
  #handler.session = Session()
  #if 'username' in handler.session:
     #newval['username'] = handler.session['username']
  handler.response.write(template.render(newval))
  return True

def createChatMember(user):
    que = ChatMember.query(ChatMember.account == user.user_id())
    # Create the ChatMember if he is not in
    if not que.fetch(limit=1):
	newuser = ChatMember(account = user.user_id(), name = user.nickname())
	newuser.put()

class MembersHandler(webapp2.RequestHandler):

  def get(self):
    user = users.get_current_user()
    if user:
	que = ChatMember.query()
	user_list = que.fetch(limit=100)
	logout_url = users.create_logout_url(self.request.path)
	template_values = {
            	'user': user.nickname(),
            	'url_logout': logout_url,
            	'url_logout_text': 'Log out',
		'user_list': user_list
        }
	doRender(self, 'members.htm',template_values)
    else:
        self.redirect(users.create_login_url(self.request.uri))

class ChatHandler(webapp2.RequestHandler):

    def get(self):
        user = users.get_current_user()
        if user:
		logout_url = users.create_logout_url(self.request.path)
		que = ChatMessage.query().order(-ChatMessage.created);
		chat_list = que.fetch(limit=10)
        	template_values = {
	          	'user': user.nickname(),
                	'url_logout': logout_url,
	           	'url_logout_text': 'Log out',
			'chat_list': chat_list
        	}
		doRender(self,'chatscreen.htm',template_values)
        else:
            self.redirect(users.create_login_url(self.request.uri))
            


    def post(self):
	    user = users.get_current_user()
	    if user:
		msg = self.request.get('message')
		latlon = self.request.get('latlon')
		country = self.request.get('country')
		city = self.request.get('city')
		logout_url = users.create_logout_url(self.request.path)
		if msg == '':
		    template_values = {
		        'user': user.nickname(),
	                'url_logout': logout_url,
			'url_logout_text': 'Log out',
			'error' : 'Blank message ignored'
		    }
		    doRender(self,'chatscreen.htm',template_values)
		else:
		    createChatMember(user)
		    if latlon != 'Error':
			newchat = ChatMessage(user = user.user_id(), text=msg, latlonpos = latlon, place = country,city = city)
		    else:
			newchat = ChatMessage(user = user.user_id(), text=msg, latlonpos = 'Error')
		    newchat.put();
		    self.get()
	    else:
		self.redirect(users.create_login_url(self.request.uri))
  


	
class MainPage(webapp2.RequestHandler):
    def get(self):
        user = users.get_current_user()
        if user:
		logout_url = users.create_logout_url(self.request.path)
        	template_values = {
		    'user': user.nickname(),
		    'url_logout': logout_url,
		    'url_logout_text': 'Log out',
        	}
		doRender(self,'index.htm',template_values)
        else:
            self.redirect(users.create_login_url(self.request.uri))
            


application = webapp2.WSGIApplication([
    ('/', MainPage),
    ('/members', MembersHandler),
    ('/chat', ChatHandler),
], debug=True)
